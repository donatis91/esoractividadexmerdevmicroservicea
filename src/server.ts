import http from "http";
import express from "express";
import "express-async-errors";
import { applyMiddleware, applyRoutes } from "./utils";
import middleware from "./middleware";
import errorHandlers from "./middleware/errorHandlers";
import routes from "./services";
import { initDependencies } from "./config/index";
import { handleBodyRequestParsing } from "./middleware/common";

process.on("uncaughtException", (e) => {
  console.log("uncaughtException");
  process.exit(1);
});

process.on("unhandledRejection", (e) => {
  console.log("unhandledRejection");

  process.exit(1);
});
const router = express();

applyMiddleware(middleware, router);
applyRoutes(routes, router);
applyMiddleware(errorHandlers, router);

const { PORT = 3000 } = process.env;
const server = http.createServer(router);

async function start() {
  await initDependencies();
  server.listen(PORT, () =>
    console.log(`Service running on: http://localhost:${PORT}...`)
  );
}

start();
