import amqp, { Channel, Message } from "amqplib/callback_api";
import dotenv from "dotenv";

dotenv.config();

let channel: Channel;
let queue = "cola";
let { ACMQP_URL = "amqp://localhost:5672" } = process.env;

const init = async () =>
  new Promise((resolved, rejected) => {
    amqp.connect(ACMQP_URL, (err, connection) => {
      if (err) {
        return rejected(err);
      }
      connection.createChannel((err, _channel) => {
        if (err) {
          console.log("Error with RabbitMQ");
          return rejected(err);
        }
        channel = _channel;
        channel.assertExchange(queue, "fanout", {
          durable: false,
        });
        console.log("Conected to RabbitMQ");
        resolved(channel);
      });
    });
  });

const subscribe = (fn: (message: any) => void) => {
  if (!channel) {
    setTimeout(() => subscribe(fn), 3000);
    return;
  }

  let channelCfg = {
    exclusive: true,
    durable: true
  };

  const callback = (msg: Message | null) => {
    if (msg) {
      fn(JSON.parse(msg.content.toString()));
    }
  };

  channel.assertQueue(queue, channelCfg, (err, q) => {
    if (err) {
      throw err;
    }
    channel.prefetch(1);
    channel.bindQueue(q.queue, queue, "");
    channel.consume(q.queue, callback, {
      noAck: false,
    });
  });
};


const publish = (message: any) => {
  // channel.publish(queue, "", Buffer.from(JSON.stringify(message))).valueOf();
  channel.sendToQueue(queue, Buffer.from(JSON.stringify(message))), {
    persistent: true
  };
};

export { init, publish, subscribe };
