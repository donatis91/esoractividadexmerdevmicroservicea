import { Request, Response, NextFunction } from "express";
import { fullData } from "./interfaces";
import { setDatos } from "./usersController";
import { HTTP400Error } from '../../utils/httpErrors';

const checkParams = (req: Request, res: Response, next: NextFunction) => {
  const { email, pass, name, lastname, phone } = req.body;
  if (!email || !pass || !name || !lastname || !phone ) {
    throw new HTTP400Error('Missing parameters: email/pass/name/lastname/phone');
  } else {
    next();
  }
};

export default [
  {
    path: "/setUser",
    method: "post",
    handler: [
      checkParams,
      (req: Request, res: Response, next: NextFunction) => {
        next();
      },
      async (req: Request, res: Response) => {
        let d: fullData = req.body;
        setDatos(d).then((d) => {
          if (d == true){
            res.status(200).send("OK");
          }else{
            res.status(500).send("Error interno");
          }
        });
      },
    ],
  },
];
