import { dbClient } from "../../config/mongodb";
import {dbData} from "./interfaces"

const users = dbClient.get("users");

export const setDatosDB = async (data: dbData) => {
  console.log("Writting on DB: ", data);
  return await users.insert(data).then((doc: any) => {
    return doc;
  });
  
};
