export interface fullData  {
    email: string,
    pass: string,
    name: string,
    lastname: string,
    phone: string
}

export interface dbData  {
    email: string,
    pass: string
}

export interface microServiceBData  {
    id: string, 
    name: string,
    lastname: string,
    phone: string
}
