import { publish } from "../../config/messenger";
import { fullData, dbData, microServiceBData } from "./interfaces";
import { setDatosDB } from "./usersModel";

export const setDatos = async (data: fullData) => {
  let datosBD: dbData = { email: data["email"], pass: data["pass"] };
  return await setDatosDB(datosBD).then((d: any) => {
    if (d) {
      let datosMicroServicvioB: microServiceBData = {
        id: d["_id"],
        name: data["name"],
        lastname: data["lastname"],
        phone: data["phone"],
      };
      publish(datosMicroServicvioB);
      return true;
    } else {
      return false;
    }
  });
};
